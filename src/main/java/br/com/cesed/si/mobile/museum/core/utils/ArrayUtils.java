package br.com.cesed.si.mobile.museum.core.utils;

import java.util.ArrayList;

public class ArrayUtils {

	private ArrayUtils() {
	}

	public static <T> ArrayList<T> toArrayList(Iterable<T> iterable) {
		ArrayList<T> ret = new ArrayList<T>();
		iterable.forEach(ret::add);
		return ret;
	}

}
