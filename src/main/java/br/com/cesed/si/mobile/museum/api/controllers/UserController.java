package br.com.cesed.si.mobile.museum.api.controllers;

import br.com.cesed.si.mobile.museum.api.dto.UserCreationDTO;
import br.com.cesed.si.mobile.museum.api.services.UserService;
import br.com.cesed.si.mobile.museum.core.domains.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@Controller
@RequestMapping("user")
public class UserController {

	@Autowired
	public UserService service;

	@GetMapping
	public ResponseEntity<List<User>> getAll() {
		return new ResponseEntity<>(service.readUsers(), HttpStatus.OK);
	}

	@GetMapping("/{id}")
	public ResponseEntity<User> getById(@PathVariable Long id) {
		return new ResponseEntity<>(service.readById(id), HttpStatus.OK);
	}

	@PostMapping
	public ResponseEntity<User> postUser(@Valid @RequestBody UserCreationDTO dto) {
		return new ResponseEntity<>(service.createUser(dto), HttpStatus.CREATED);

	}

	@PutMapping("/{id}")
	public ResponseEntity<User> putUser(@PathVariable Long id, @RequestBody User user) {
		return new ResponseEntity<>(service.updateUser(id, user), HttpStatus.OK);
	}

	@DeleteMapping("/{id}")
	public ResponseEntity<Void> deleteUser(@PathVariable Long id) {
		service.deleteUser(id);
		return ResponseEntity.ok().build();

	}
}