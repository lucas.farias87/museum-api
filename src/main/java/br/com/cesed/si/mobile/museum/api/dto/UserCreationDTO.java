package br.com.cesed.si.mobile.museum.api.dto;

import lombok.Data;

@Data
public class UserCreationDTO {

    private String name;
    private String phone;
    private String zipCode;
    private String street;
    private String number;
    private String neighborhood;
    private String complement;
    private String username;
    private String password;

}
