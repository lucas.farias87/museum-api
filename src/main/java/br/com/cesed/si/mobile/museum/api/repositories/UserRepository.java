package br.com.cesed.si.mobile.museum.api.repositories;

import br.com.cesed.si.mobile.museum.core.domains.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {

}
