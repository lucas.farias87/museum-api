package br.com.cesed.si.mobile.museum.core.domains;

import javax.persistence.*;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Beacon {

	@Id
	@GeneratedValue
	private Long beaconId;

	private String imageUrl;

	@Column(columnDefinition="text")
	private String description;

}
