package br.com.cesed.si.mobile.museum.api.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.cesed.si.mobile.museum.core.domains.Beacon;

@Repository
public interface BeaconRepository extends JpaRepository<Beacon, Long> {

}
