package br.com.cesed.si.mobile.museum.api.controllers;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import br.com.cesed.si.mobile.museum.api.dto.VisitList;
import br.com.cesed.si.mobile.museum.api.services.VisitService;
import br.com.cesed.si.mobile.museum.core.domains.Visit;

@Controller
@RequestMapping("/visits")
public class VisitController {

	@Autowired
	public VisitService service;

	@GetMapping
	public ResponseEntity<List<VisitList>> getAll() {
		return new ResponseEntity<List<VisitList>>(service.findAll(), HttpStatus.OK);

	}

	@GetMapping("/{id}")
	public ResponseEntity<Visit> getById(@PathVariable Long id) {

		Visit visit = service.readById(id);
		return new ResponseEntity<Visit>(visit, HttpStatus.OK);

	}

	@PostMapping
	public ResponseEntity<Visit> postVisitor(@Valid @RequestBody Visit visit) {

		Visit body = service.createVisitor(visit);
		return new ResponseEntity<Visit>(body, HttpStatus.OK);

	}

	@PutMapping("/{id}")
	public ResponseEntity<Visit> putVisitor(@PathVariable Long id, @RequestBody Visit visit) {

		Visit body = service.updateVisitor(id, visit);
		return new ResponseEntity<Visit>(body, HttpStatus.OK);

	}

	@DeleteMapping("/{id}")
	public ResponseEntity<?> deleteVisitor(@PathVariable Long id) {

		service.deleteVisitor(id);
		return ResponseEntity.ok().build();

	}

}
