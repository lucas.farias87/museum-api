package br.com.cesed.si.mobile.museum;

import br.com.cesed.si.mobile.museum.api.repositories.BeaconRepository;
import br.com.cesed.si.mobile.museum.core.domains.Beacon;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.data.web.config.EnableSpringDataWebSupport;

import java.util.Optional;

@SpringBootApplication
@EnableSpringDataWebSupport
public class MuseumApplication {
    public static void main(String[] args) {
        SpringApplication.run(MuseumApplication.class, args);
    }

    @Bean
    CommandLineRunner runner(BeaconRepository repository) {
        return (args) -> {
            Optional<Beacon> beaconOpt = repository.findById(1L);
            if (!beaconOpt.isPresent()) {
                repository.save(new Beacon(
                        1L,
                        "https://museum-bucket.s3-sa-east-1.amazonaws.com/test.jpg",
                        "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum."
                        )
                );
            }
        };
    }

}
