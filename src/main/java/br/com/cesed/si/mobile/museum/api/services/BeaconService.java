package br.com.cesed.si.mobile.museum.api.services;

import java.util.List;

import br.com.cesed.si.mobile.museum.api.repositories.BeaconRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.cesed.si.mobile.museum.core.domains.Beacon;

@Service
public class BeaconService {

	@Autowired
	private BeaconRepository repository;

	public Beacon createBeacon(Beacon beacon) {

		return this.repository.save(beacon);

	}

	public Beacon updateBeacon(Long id, Beacon beacon) {

		beacon.setBeaconId(id);
		return this.repository.save(beacon);

	}

	public void deleteBeacon(Long id) {

		this.repository.deleteById(id);

	}

	public List<Beacon> readBeacons() {

		return (List<Beacon>) this.repository.findAll();

	}

	public Beacon readById(Long id) {
		return repository.findById(id).get();
	}

}
