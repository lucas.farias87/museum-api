package br.com.cesed.si.mobile.museum.api.controllers;

import java.util.List;

import javax.validation.Valid;

import br.com.cesed.si.mobile.museum.api.services.BeaconService;
import br.com.cesed.si.mobile.museum.core.domains.Beacon;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/beacon")
public class BeaconController {

	@Autowired
	public BeaconService service;

	@GetMapping
	public ResponseEntity<List<Beacon>> getAll() {

		List<Beacon> body = service.readBeacons();
		return new ResponseEntity<List<Beacon>>(body, HttpStatus.OK);

	}

	@GetMapping("/{id}")
	public ResponseEntity<Beacon> getById(@PathVariable Long id) {
		return new ResponseEntity<>(service.readById(id), HttpStatus.OK);
	}

	@PostMapping
	public ResponseEntity<Beacon> postBeacon(@Valid @RequestBody Beacon beacon) {

		Beacon body = service.createBeacon(beacon);
		return new ResponseEntity<Beacon>(body, HttpStatus.OK);

	}

	@PutMapping("/{id}")
	public ResponseEntity<Beacon> putBeacon(@PathVariable Long id, @RequestBody Beacon beacon) {

		Beacon body = service.updateBeacon(id, beacon);
		return new ResponseEntity<Beacon>(body, HttpStatus.OK);

	}

	@DeleteMapping("/{id}")
	public ResponseEntity<?> deleteBeacon(@PathVariable Long id) {

		service.deleteBeacon(id);
		return ResponseEntity.ok().build();

	}
}
