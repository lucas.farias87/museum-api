package br.com.cesed.si.mobile.museum.api.services;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.cesed.si.mobile.museum.api.dto.VisitList;
import br.com.cesed.si.mobile.museum.api.repositories.VisitorRepository;
import br.com.cesed.si.mobile.museum.core.domains.User;
import br.com.cesed.si.mobile.museum.core.domains.Visit;

@Service
public class VisitService {

	@Autowired
	private VisitorRepository repository;

	@Autowired
	private UserService userService;

	public Visit createVisitor(Visit visit) {

		return this.repository.save(visit);

	}

	public Visit updateVisitor(Long id, Visit visit) {

		visit.setId(id);
		return this.repository.save(visit);

	}

	public void deleteVisitor(Long id) {

		this.repository.deleteById(id);

	}

	public List<Visit> readVisitors() {

		return this.repository.findAll();

	}

	public Visit readById(Long id) {

		return this.repository.findById(id).get();

	}

	public List<VisitList> findAll() {
		List<VisitList> listVisit = new ArrayList<>();
		userService.readUsers().forEach(user -> {
			listVisit.addAll(getVisits(user));
		});
		return listVisit;
	}

	private static List<VisitList> getVisits(User user) {
		return user.getVisits().stream().map(visit -> getVisit(user, visit)).collect(Collectors.toList());
	}

	private static VisitList getVisit(User user, Visit visit) {
		VisitList visitList = new VisitList();

		visitList.setId(visit.getId());
		visitList.setHora(visit.getVisitingTime());
		visitList.setEmail(user.getUsername());
		visitList.setPhoneIdnumber(user.getId());
		visitList.setPhonenumber(user.getPhone());
		visitList.setUsuario(user.getName());

		return visitList;
	}

}
