package br.com.cesed.si.mobile.museum.api.services;

import java.util.List;

import br.com.cesed.si.mobile.museum.api.dto.UserCreationDTO;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.cesed.si.mobile.museum.api.repositories.UserRepository;
import br.com.cesed.si.mobile.museum.core.domains.User;

@Service
public class UserService {

	@Autowired
	private UserRepository repository;

	@Autowired
	private ModelMapper modelMapper;

	public User createUser(UserCreationDTO dto) {
		return repository.save(toEntity(dto));
	}

	private User toEntity(UserCreationDTO dto) {
		return modelMapper.map(dto, User.class);
	}

	public User updateUser(Long id, User user) {

		user.setId(id);
		return this.repository.save(user);

	}

	public void deleteUser(Long id) {

		this.repository.deleteById(id);

	}

	public List<User> readUsers() {

		return (List<User>) this.repository.findAll();

	}

	public User readById(Long id) {

		return this.repository.findById(id).get();

	}

}
