package br.com.cesed.si.mobile.museum.api.dto;

import java.time.LocalDateTime;

import lombok.Data;

@Data
public class VisitList {

	private Long id;
	private LocalDateTime hora;
	private String usuario;
	private String email;
	private String phonenumber;
	private Long phoneIdnumber;

}
