package br.com.cesed.si.mobile.museum.core.domains;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@NoArgsConstructor
@Table(name = "user_museum")
public class User {

	@Id
	@GeneratedValue
	private Long id;

	@OneToMany(cascade = CascadeType.ALL)
	@JoinColumn(name = "user_id")
	private List<Visit> visits;
	
	@Column(nullable = false)
	private String name;

	@Column(nullable = false)
	private String username;

	@Column(length = 15)
	private String phone;

	@Column(length = 15)
	private String zipCode;

	private String street;
	
    private String number;

	private String neighborhood;

	private String complement;
	
}
