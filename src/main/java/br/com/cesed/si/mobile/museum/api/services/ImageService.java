package br.com.cesed.si.mobile.museum.api.services;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

import javax.imageio.ImageIO;

import org.apache.commons.io.FilenameUtils;
import org.imgscalr.Scalr;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import br.com.cesed.si.mobile.museum.api.exceptions.FileException;

@Service
public class ImageService {

	public BufferedImage getImageFromFile(MultipartFile uploadedFile) {

		if (uploadedFile == null) {
			throw new FileException("Imagem não presente.");
		}

		String ext = FilenameUtils.getExtension(uploadedFile.getOriginalFilename());

		if (!"png".equals(ext) && !"jpg".equals(ext)) {
			throw new FileException("Somente imagens PNG são permitidas.");
		}

		try {
			return ImageIO.read(uploadedFile.getInputStream());
		} catch (IOException e) {
			throw new FileException("Erro ao ler arquivo.");
		}

	}

	public InputStream getInputStream(BufferedImage img, String extension) {
		try {
			ByteArrayOutputStream os = new ByteArrayOutputStream();
			ImageIO.write(img, extension, os);
			return new ByteArrayInputStream(os.toByteArray());
		} catch (IOException e) {
			throw new FileException("Erro ao ler arquivo");
		}
	}

	public BufferedImage resize(BufferedImage sourceImg, int size) {
		return Scalr.resize(sourceImg, Scalr.Method.ULTRA_QUALITY, size);
	}

}
