package br.com.cesed.si.mobile.museum.api.exceptions;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class StandardError {

	private Integer status;

	private String error;

	private String message;

	private String path;

}
