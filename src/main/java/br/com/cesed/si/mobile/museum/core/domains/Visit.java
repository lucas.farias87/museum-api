package br.com.cesed.si.mobile.museum.core.domains;

import java.time.LocalDateTime;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@NoArgsConstructor
public class Visit {

	@Id
	@GeneratedValue
	private Long id;
	
	private LocalDateTime visitingTime;

}
