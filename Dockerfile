FROM maven:3.5-jdk-8 AS build  
COPY src /usr/src/api/src  
COPY pom.xml /usr/src/api  
RUN mvn -f /usr/src/api/pom.xml -Dmaven.test.skip=true clean package

FROM java:8
COPY --from=build /usr/src/api/target/**.jar /usr/api/museum.jar 
EXPOSE 8080
ENTRYPOINT ["java","-jar", "/usr/api/museum.jar"]